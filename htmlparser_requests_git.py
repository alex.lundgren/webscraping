import requests
from bs4 import BeautifulSoup
import json
import random
import string
import mysql.connector
from mysql.connector import Error

def create_connection(host_name, user_name, user_password, db_name):
    connection = None
    try:
        connection = mysql.connector.connect(
            host=host_name,
            user=user_name,
            passwd=user_password,
            database=db_name
        )
        print("Connection to MySQL DB successful")
    except Error as e:
        print(f"The error '{e}' occurred")

    return connection

def execute_query(connection, query):
    cursor = connection.cursor()
    try:
        cursor.execute(query)
        connection.commit()
        print("Query executed successfully")
    except Error as e:
        print(f"The error '{e}' occurred")

connection = create_connection("host", "user", "passwd", "database")

link = "https://biluppgifter.se/login"

session = requests.Session()
response = session.get(link)
token_value = BeautifulSoup(response.text, 'html.parser').select_one('form input[name="_token"]').get('value')
payload = {
    "email": "user",
    "password": "passwd",
    "_token": token_value,
    'location[latitude]': None,
    'location[longitude]': None
}
headers = {
    "Accept-Encoding": "gzip, deflate, br",
    "Connection": "keep-alive",
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36",
    "Referer": "https://biluppgifter.se/login",
    "Origin": "https://biluppgifter.se"
}
session.headers.update(headers)

login = session.post("https://biluppgifter.se/login",data=payload)
print(login)

headers = {
    "Accept-Encoding": "utf-8",
    "Connection": "keep-alive",
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36",
    "Referer": "https://biluppgifter.se/fordon/",
    "Origin": "https://biluppgifter.se/login"
}
session.headers.update(headers)

for i in range(10):
    random_letters = ''.join(random.choice(string.ascii_uppercase) for i in range(3))
    random_numbers = ''.join(random.choice(string.digits) for i in range(3))
    randomized_reg = random_letters + random_numbers
    my_reg = 'RHL396'
    print(randomized_reg)

    url = f"https://biluppgifter.se/fordon/{randomized_reg}"
    page = session.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')

    #with open("soup_file4.html", "w") as soup_file:
     #   soup_file.write(str(soup))
    try:
        chassi = soup.find('span', class_ = 'label', string = 'Kaross').find_next_sibling('span', class_ = 'value').string
    except Exception as e:
        print(f'Wrong chassi data. Error {e} occurred, continuing...')
        continue

    try:
        transmission = soup.find('span', class_ = 'label', string = 'Växellåda').find_next_sibling('span', class_ = 'value').string
    except Exception as e:
        print(f'No transmission data. Error {e} occurred, transmission = NULL')
        transmission = None
        


    if (
        chassi == 'Släp' or
        chassi == 'Lastbil' or
        chassi == 'Moped' or
        chassi == 'Motorcykel' or
        chassi == 'Traktor' or
        chassi == 'Terränghjuling' or
        chassi == 'Motorredskap' or
        chassi == 'Terrängskoter' or
        chassi == 'Snöskoter' or
        chassi == 'Terrängvagn' or
        chassi == 'Buss' or
        chassi == 'Husvagn'
    ):
        continue
    else:
        try:
            title = soup.find('title').string.split(',')
            car_info = json.loads(soup.find('script', id = 'gtm_reference').string.strip())

            model_year = car_info['modelYear']
            make = car_info['make']
            model = car_info['model']
            colour = soup.find('span', class_ = 'label', string = 'Färg').find_next_sibling('span', class_ = 'value').string
            hp = title[1].strip().split(' ')[2][:-2]
            reg = soup.find('span', class_ = 'label', string = 'Registreringsnummer').find_next_sibling('span', class_ = 'value').string
            vin = soup.find('span', class_ = 'label', string = 'Chassinr / VIN').find_next_sibling('span', class_ = 'value').string
            fuel = car_info['fuelType']
            awd = soup.find('span', class_ = 'label', string = 'Fyrhjulsdrift').find_next_sibling('span', class_ = 'value').string
        except:
            continue
        

        create_row = f"INSERT INTO bilar (VIN, reg, year, make, model, colour, fuel_system, transmission, hp, awd, chassi) VALUES ('{vin}','{reg}',{int(model_year)},'{make}','{model}','{colour}','{fuel}',{'Default' if transmission == None else '{transmission}'},{int(hp)},'{awd}','{chassi}');"
        execute_query(connection, create_row)

        """print(title)
        print(model_year)
        print(make)
        print(model)
        print(colour)
        print(hp)
        print(reg)
        print(vin)
        print(fuel)
        print(chassi)
        print(transmission)
        print(awd)"""